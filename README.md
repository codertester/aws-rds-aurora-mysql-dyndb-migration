---
# Create Database Migration Resources with AWS TypeScript CDK
---
The repo contains modules for creating, the following resources with **AWS TypeScript CDK**, for database migration projects:


1. AWS VPC.    

2. AWS VPC-related resources.                                                                                
                                                                                                           
3. AWS DynamoDB Table.  

4. AWS Secret Manager's Secret (username and password), for the DB Cluster.

5. AWS RDS Aurora (MySQL-compatible) DB Cluster.

6. AWS S3 Buckets used, for Import and Export, by AWS RDS Aurora (MySQL-compatible) DB Cluster.      